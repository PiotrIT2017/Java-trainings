package engine.pp.proj2;

import java.util.Scanner;

public class Proj2 {

        public static void main(String[] args) {
            Scanner person=new Scanner(System.in);
            System.out.println("Podaj imię:");
            String name=person.next();
            System.out.println("Witaj! "+name);
            System.out.println("Prosze podac liczbe:");
            int podanaLiczba = getInt();
            System.out.println("Teraz policzymy kwadrat tej liczby.");
            double liczbaDoKwadratu = policzKwadrat(podanaLiczba);
            System.out.println("Kwadrat tej liczby wynosi " + liczbaDoKwadratu);
            if(czyPierwsza(podanaLiczba)) {
                System.out.println("Liczba jest pierwsza.");
            }else{
                System.out.println("Liczba nie jest pierwsza.");
            }
            if(czyParzysta(podanaLiczba)) {
                System.out.println("Liczba jest parzysta.");
            }else{
                System.out.println("Liczba nie jest parzysta.");
            }

        }



        public static int getInt() {
            return new Scanner(System.in).nextInt();
        }

        public static double policzKwadrat(int liczba) {
            return liczba * liczba;
        }

        public static boolean czyPierwsza(int liczba)
        {
            if(liczba<2)
            {
                return false;
            }
            for(int i=2; i<=liczba/2; i++)
            {
                if(liczba%i==0)
                {
                    return false;
                }
            }
            return true;
        }

    public static boolean czyParzysta(int liczba)
    {
        if(liczba%2==0)
        {
            return true;
        }
        return false;
    }


}
