package engine.pp;

import java.lang.Math;
import java.util.Scanner;

public class proj4 {
    
    public static void main(String[] args) {

        char operator;
        String function;

        Double number1, number2, result;

        // create an object of Scanner class
        Scanner input = new Scanner(System.in);
        Scanner input2 = new Scanner(System.in);

        // ask users to enter operator
       // System.out.println("Choose an operator: +, -, *, or /");
       // operator = input.next().charAt(0);



        // ask users to enter numbers
        System.out.println("Enter first number");
        number1 = input.nextDouble();

        System.out.println("Enter second number");
        number2 = input.nextDouble();

        // ask users to enter operator
        System.out.println("Choose an operator: MIN, MAX, SQRT, or ABS");
        function = input.next();

        switch(function){
            case "MIN":
                System.out.println("Minimum wynosi: "+Math.min(number1,number2));
                break;
            case "MAX":
                System.out.println("Maximum wynosi: "+Math.max(number1,number2));
                break;
            case "SQRT":
                System.out.println("Pierwiastek z liczby1: "+Math.sqrt(number1)+" z liczby2: "+Math.sqrt(number2));
                break;
            case "ABS":
                System.out.println("Wartość bezwzględna z liczby1: "+Math.abs(number1)+" z liczby2: "+Math.abs(number2));
                break;
            default:
                System.out.println("Invalid operator!");
                break;
        }
/*
        switch (operator) {

            // performs addition between numbers
            case '+':
                result = number1 + number2;
                System.out.println(number1 + " + " + number2 + " = " + result);
                break;

            // performs subtraction between numbers
            case '-':
                result = number1 - number2;
                System.out.println(number1 + " - " + number2 + " = " + result);
                break;

            // performs multiplication between numbers
            case '*':
                result = number1 * number2;
                System.out.println(number1 + " * " + number2 + " = " + result);
                break;

            // performs division between numbers
            case '/':
                result = number1 / number2;
                System.out.println(number1 + " / " + number2 + " = " + result);
                break;

            default:
                System.out.println("Invalid operator!");
                break;
        }
*/
        input.close();

    }
}
