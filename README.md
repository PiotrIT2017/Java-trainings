## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)

## General info
* proj3 - an implementation of the thermostat
* proj4 - an implementation of the calculator
* proj6 - an example of encapsulation

## Technologies
Project is created with:
* Java version: 18.0.2
