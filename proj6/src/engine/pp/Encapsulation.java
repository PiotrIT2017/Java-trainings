package engine.pp;


import java.util.Arrays;
import java.util.Random;

public class Encapsulation {

    private int i=0;

    public void setI(int i) {
        this.i = i;
    }

    public int getI() {
        return i;
    }

    public static void main(String[] args) {

        Encapsulation size=new Encapsulation();
        size.setI(5);
        int[] tab=new int[size.getI()];
        Random r =new Random();
        int n= r.nextInt(5);
        for(int i=0;i<tab.length;i++){
            tab[i]=n;
        }

     System.out.println(Arrays.toString(tab));

    }
}
